# terraform-aws-s3-notifications

A Terraform module to manage notifications for S3 object creation events. This should be deployed in the account where the S3 bucket resides, unless you're willing to deal with cross-account requirements, which are out-of-scope for this module.

![System diagram](https://gitlab.com/fer1035_terraform/modules/terraform-aws-s3-notifications/-/raw/main/images/diagram.png)

## Main Resources

- S3 bucket notification
- SQS queue to receive S3 notifications
- IAM user and role to read messages from the queue and fetch data from S3

## Use-Case References

- [Cribl Stream](https://docs.cribl.io/stream/sources-s3/)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.40.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user.user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy_attachment.user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_s3_bucket_notification.bucket_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification) | resource |
| [aws_sqs_queue.queue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue) | resource |
| [aws_sqs_queue_policy.queue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue_policy) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.queue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_principals"></a> [assume\_role\_principals](#input\_assume\_role\_principals) | The ARNs and / or IDs of the principals that can assume the role. | `list` | `[]` | no |
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | The name of the S3 bucket. | `string` | n/a | yes |
| <a name="input_external_id"></a> [external\_id](#input\_external\_id) | The external ID to use in the role's trust relationship. | `string` | `null` | no |
| <a name="input_queue_filter_prefix"></a> [queue\_filter\_prefix](#input\_queue\_filter\_prefix) | A prefix to filter the events that should trigger the SQS queue. | `string` | `null` | no |
| <a name="input_queue_filter_suffix"></a> [queue\_filter\_suffix](#input\_queue\_filter\_suffix) | A suffix to filter the events that should trigger the SQS queue. | `string` | `null` | no |
| <a name="input_queue_id"></a> [queue\_id](#input\_queue\_id) | A unique ID to identify the SQS queue. | `string` | n/a | yes |
| <a name="input_user_path"></a> [user\_path](#input\_user\_path) | The path for the IAM user. | `string` | `"/"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_s3_notification_details"></a> [s3\_notification\_details](#output\_s3\_notification\_details) | The details of the S3 notification. |
