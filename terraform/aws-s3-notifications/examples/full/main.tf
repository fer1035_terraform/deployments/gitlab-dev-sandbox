provider "aws" {
  region = "us-east-1"
}

module "s3_notifications" {
  source  = "app.terraform.io/my-org/s3-notifications/aws"
  version = "1.0.3"

  bucket_name         = "my-bucket"
  queue_id            = "my-logs"
  queue_filter_prefix = "AWSLogs/"
  queue_filter_suffix = ".json.gz"
  user_path           = "/"
  external_id         = "123456789012"

  assume_role_principals = [
    "arn:aws:iam::123456789012:role/my-role"
  ]
}

output "s3_notification_details" {
  value = module.s3_notifications.s3_notification_details
}
