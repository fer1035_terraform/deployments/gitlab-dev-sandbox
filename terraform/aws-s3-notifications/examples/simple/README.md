# terraform-aws-s3-notifications

Simple example:

```hcl
provider "aws" {
  region = "us-east-1"
}

module "s3_notifications" {
  source = "app.terraform.io/my-org/s3-notifications/aws"

  bucket_name = "my-bucket"
  queue_id    = "my-logs"
}

output "s3_notification_details" {
  value = module.s3_notifications.s3_notification_details
}

```
