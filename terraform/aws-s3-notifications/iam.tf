data "aws_iam_policy_document" "policy" {
  statement {
    effect = "Allow"

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ChangeMessageVisibility"
    ]

    resources = [
      aws_sqs_queue.queue.arn
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}"
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}

resource "aws_iam_policy" "policy" {
  name        = "${var.queue_id}-policy"
  description = "S3 notifications policy."
  policy      = data.aws_iam_policy_document.policy.json
}

resource "aws_iam_user" "user" {
  name = "${var.queue_id}-user"
  path = var.user_path
}

resource "aws_iam_user_policy_attachment" "user" {
  user       = aws_iam_user.user.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_role" "role" {
  name = "${var.queue_id}-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""

        Principal = {
          AWS = var.assume_role_principals != [] ? var.assume_role_principals : ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
        }

        Condition = {
          StringEquals = {
            "sts:ExternalId" = var.external_id != null ? var.external_id : data.aws_caller_identity.current.account_id
          }
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "role" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}
