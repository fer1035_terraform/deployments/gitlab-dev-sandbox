provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      env  = "sandbox"
      tier = "dev"
    }
  }
}

# module "network" {
#   source = "app.terraform.io/fer1035/network/aws"
# }

# module "nacl" {
#   source = "app.terraform.io/fer1035/network-acl/aws"
#   vpc_id = "vpc-065b8580f36e5bef5"

#   subnet_ids = [
#     "subnet-0b8851399f9c985dc",
#     "subnet-0c31c90189fe82dc7",
#     "subnet-0bf3b91d00973af0f",
#     "subnet-09db5956011a4f9a3"
#   ]
# }

# output "nacl" {
#   value = module.nacl.nacl
# }

# module "s3_notifications" {
#   source = "./aws-s3-notifications"

#   bucket_name         = "ferdaus-uploads"
#   queue_id            = "my-logs"
# }

# output "s3_notification_details" {
#   value = module.s3_notifications.s3_notification_details
# } 
